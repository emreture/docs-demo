# H1 Header
This paragraph is under h1 header.

## H2 Header
This paragraph is under h2 header and a little bit longer.

### H3 Header
And this paragraph is under h3 header. I wonder how it is rendered under different CSS sytles...

#### H4 Header
Under this header there are two paragraphs. This is the first one.

And this is the second and the last one.
##### H5 Header
Just like the Red Leader said, Almost There. But he wasn't successful.

We are running out of headers...
###### H6 Header
Finally there we are!! This is the last header.

This presentation will end with a horizantal ruler `hr`

---

# Extended Markdown
## Strike-through
Next sentence is striked through. ~~I decided to remove this sentence~~. Now back to normal.

## Highlights
This sentence is important. ==But this sentence is more important.== Back to no-highlight.

## Emoji
This is a cat. :octocat:

This is a star. :star:

---
EOF
